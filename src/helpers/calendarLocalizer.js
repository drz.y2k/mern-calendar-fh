import { dateFnsLocalizer } from 'react-big-calendar';
// importamos las funciones que necesitaremos de date-fns
//este código se está tomando de la documentación de react-big-calendar
import {  format, parse, startOfWeek, getDay } from 'date-fns';

//importamos el idioma ingles de la libreria date-fns
import enUS from 'date-fns/locale/en-US';
import esES from 'date-fns/locale/es';



//definimos el archivo locales, para poder usar el idioma en el calendario
const locales = {
	'en-US': enUS,
	'es': esES,
};

//usamos la funcion localizer con las funciones de date-fns
//y el objeto de idiomas
export const localizer = dateFnsLocalizer({
	format,
	parse,
	startOfWeek,
	getDay,
	locales,
});