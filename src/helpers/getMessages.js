//esta funcion retorna valores que se usarán en los textos del calendario, para poder ponerlos
//en español
export const getMessagesES = () => {
	return {
		allDay: 'Todo el día',
		previous: '<',
		next: '>',
		today: 'Hoy',
		month: 'Mes',
		week: 'Semana',
		day: 'Día',
		agenda: 'Agenda',
		date: 'Fecha',
		time: 'Hora',
		event: 'Evento',
		noEventsInRange: 'No hay eventos en este rango',
		showMore: total => `+ Ver más (${total})`
	};
}
