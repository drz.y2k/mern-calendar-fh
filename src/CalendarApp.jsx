import { BrowserRouter } from 'react-router-dom';
import { AppRouter } from './router';
import { Provider } from 'react-redux';
import { store } from './store';

const CalendarApp = () => {
	return (
		<>
			{/* Ponemos a este nivel nuestro browserRouter necesario para poder utilizar, los componentes de react-router,
      las rutas estan definidas en AppRouter, y este es el componente principal de la aplicacion
      por tanto es el que se cargará en el main  */}
			<Provider store={store}>
				<BrowserRouter>
					<AppRouter />
				</BrowserRouter>
			</Provider>
		</>
	);
};

export default CalendarApp;
