//importamos nuestro useDispatch de redux
//y nuestra función para asignar el evento activo de nuestro store
import { useDispatch, useSelector } from "react-redux"
import { onAddNewEvent, onDeleteEvent, onSetActiveEvent, onUpdateEvent } from "../store";
//Creamos nuestro hook para obtener nuestros estados del store
//sin tener que importar los slices en cada componente
//y nuestro selector de redux, simplemente exportando
//las propiedades en este hook

export const useCalendarStore = () => {

	//asignamos la variable que hace referencia a nuestro dispatch de redux
	const dispatch = useDispatch();

	//Obtenemos nuestros estados del store de redux de la parte del calendario
	//y las desestructuramos para exportarlas en este hook
	const { events, activeEvent } = useSelector((state) => state.calendar);

	//creamos la función que recibe un evento y lo asigna a la propiedad activeEvent
	//de nuestro store de redux haciendo uso del dispatch, esto para evitarnos
	//importar el slice en cada componente y todo lo relacionado a redux
	//aqui llamamos la función onSetActiveEvent que se encuentra en nuestro slice de redux
	//y le pasamos el evento que queremos asignar a la propiedad activeEvent
	const setActiveEvent = (calendarEvent) => {
		dispatch(onSetActiveEvent(calendarEvent));
	}

	//creamos la función startSavingEvent que recibe un evento y lo guarda en el store
	//de momento solamente lo agrega a nuestro store, estamos aquí haciendo una pequeña validación
	//ya que si contamos con el id significa que es una edición porque es un evento que ya existe
	//en caso contrario es un evento nuevo y le asignamos un id con un timestamp 
	//que posteriormente no se necesitará porque la información vendrá del backend
	//y aquí solo se agregará ese valor del backend a nuestro store local
	const startSavingEvent = async (calendarEvent) => {
		//TODO llegar al backend

		//si TODO bien

		if (calendarEvent._id) {
			//Actualizando
			//llamamos al dispatch y le pasamos la función onUpdateEvent
			//junto con una copia del objeto calendarEvent, mediante el uso de spread operator,
			//dentro la función del store manejará la lógica para actualizar el evento
			dispatch(onUpdateEvent({...calendarEvent}));
		} else {
			//Creando
			//hacemos spread para romper la referencia y hacer una copia del objeto
			dispatch(onAddNewEvent({ ...calendarEvent, _id: new Date().getTime() }));
		}
	}

	//creamos la función startDeletingEvent
	//esta simplemente hace el dispatch de la función onDeleteEvent
	//que está en nuestro store, de momento no hace la validación en el backend de la eliminación
	//aunque es algo que se hará en el futuro
	const startDeletingEvent=()=>{
		//Todo llegar al backend
		//si todo bien
		dispatch(onDeleteEvent())
	}

	//exportamos las propiedades de nuestro store
	//así como también nuestros métodos
	return {
		//Propiedades
		events,
		activeEvent,
		//validamos que haya un evento activo
		//usando la doble negación para convertir el valor a booleano
		hasEventSelected:!!activeEvent,

		//Métodos
		startDeletingEvent,
		setActiveEvent,
		startSavingEvent,

	}
}