import { useSelector, useDispatch } from "react-redux"
import { onOpenDateModal, onCloseDateModal } from "../store"

//creamos un hook donde haremos los llamados a los dispatch de nuestro store

export const useUiStore = () => {
	//usamos el hook useDispatch para poder llamar a los dispatch de nuestro store
	const dispatch = useDispatch();

	//usamos el hook useSelector para poder acceder a las propiedades de nuestro store
	//en este caso del slice ui, accedemos a la propiedad isDateModalOpen
	//que sirve para saber si el modal de fecha está abierto o cerrado
	const { isDateModalOpen } = useSelector(state => state.ui);

	//creamos los métodos que necesitamos para abrir, cerrar y alternar el modal de fecha
	//estos métodos llaman a los dispatch de nuestro store
	//con las funciones onOpenDateModal y onCloseDateModal que están en el slice ui
	const openDateModal = () => {
		dispatch(onOpenDateModal());
	}

	const closeDateModal = () => {
		dispatch(onCloseDateModal());
	}

	//este método es para alternar el modal de fecha
	//si está abierto lo cierra y si está cerrado lo abre
	const toggleDateModal = () => {
		isDateModalOpen ? closeDateModal() : openDateModal();
	}

	return {
		//Propiedades
		isDateModalOpen,

		//Métodos
		openDateModal,
		closeDateModal,
		toggleDateModal

	}
}