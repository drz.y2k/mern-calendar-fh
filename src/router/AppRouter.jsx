import { Navigate, Route, Routes } from 'react-router-dom';
import { LoginPage } from '../auth';
import { CalendarPage } from '../calendar';

export const AppRouter = () => {
	// const authStatus = 'not-authenticated';
	//definimos esta variable para simular estar o no autenticado y retornar rutas
	//en base a esa condicion
	const authStatus = 'authenticated';

	return (
		//definimos nuestras rutas en este objeto
		<Routes>
			{/* //si no estamos autenticados, retornamos solo nuestra ruta */}
			{authStatus === 'not-authenticated' ? (
				<Route path="/auth/*" element={<LoginPage />} />
			) : (
				<Route path="/*" element={<CalendarPage />} />
			)}

			{/* failsafe para evitar que llegue a una ruta que no existe */}
			<Route path="/*" element={<Navigate to="/auth/login" />} />
		</Routes>
	);
};
