//Importamos el componente calendario y la funcion localizer
//hay una funcion de localizer para varias librerias, en este caso
//para la libreria date-fns se usa esta de nombre dateFnsLocalizer
import { Calendar } from 'react-big-calendar';
//importamos los estilos css del calendario
import 'react-big-calendar/lib/css/react-big-calendar.css';

// import { addHours } from 'date-fns';

import { Navbar, CalendarEvent, CalendarModal, FabAddNew, FabDelete } from '../';
import { localizer, getMessagesES } from '../../helpers';
import { useState } from 'react';
//importamos nuestro hook useUiStore
import { useUiStore,useCalendarStore } from '../../hooks';

//creamos un objeto de eventos con un evento de ejemplo
//title y start y end son obligatorios, los otros campos son personalizados por el usuario
//y pueden contener la informacion que el usuari quiera como en este caso
//incluso manadmos un objeto de usuario y un color de fondo como propiedades
//comentamos nuestro evento temporal de prueba porque ahora lo pasamos a nuestro store y lo creamos allá
// const events = [
// 	{
// 		title: 'Cumpleaños del jefe',
// 		notes: 'Hay que comprar el pastel',
// 		start: new Date(),
// 		end: addHours(new Date(), 2),
// 		bgColor: '#fafafa',
// 		user: {
// 			_id: '123',
// 			name: 'Fernando',
// 		},
// 	},
// ];

const CalendarPage = () => {
	//usamos nuestro hook useUiStore para obtener los métodos que necesitamos
	//en este caso solo necesitamos el método openDateModal
	const { openDateModal } = useUiStore();
	//usamos nuestro hook useCalendarStore para obtener los eventos de nuestro calendario
	//en vez de obtenerlos del objeto temporal que habiamos creado en este archivo, este hook
	//los consulta de nuestro slice de redux
	const { events,setActiveEvent } = useCalendarStore();

	//creamos un estado para guardar la última vista que usó el usuario en el calendario
	//asignamos que su valor inicial sea el que tenga en el localStorage o si no tiene nada
	//asignamos el valor de 'week' que será la vista por defecto del calendario
	const [lastView, setLastView] = useState(
		localStorage.getItem('lastView') || 'week'
	);

	//esto nos ayuda a proporcionar estilos a nuestro calendario, lo que tenemos en el objeto style
	//se va a aplicar al evento de nuestro calendario, al div que contiene el titulo del evento
	const eventStyleGetter = (event, start, end, isSelected) => {
		// console.log({ event, start, end, isSelected });

		const style = {
			backgroundColor: '#367CF7',
			borderRadius: '0px',
			opacity: 0.8,
			color: 'white',
			// fontWeight: 'bold',
		};

		return {
			style,
		};
	};

	//creamos los métodos de calendario que usaremos cuando ejecutemos acciones
	//sobre el calendario como click, doble click o el cambio en la vista del calendario

	const onDoubleClick = (event) => {
		// console.log({ doubleClick: event });
		//cuando hagamos doble click en un evento, abrimos el modal de fecha
		//usando el método openDateModal que obtuvimos de nuestro hook useUiStore
		openDateModal();
	};

	const onSelect = (event) => {
		console.log({ click: event });
		//de la función de nuestro hook useCalendarStore, asignamos el evento activo
		//que seleccionamos en el calendario, con la función setActiveEvent
		//esta función recibe el evento que seleccionamos en el calendario, esto lo enviamos a nuestro 
		//hook para que se asigne en el store
		setActiveEvent(event);
	};

	const onViewChanged = (event) => {
		//cuando cambiemos la vista del calendario, guardamos en el localStorage
		//y actualizamos nuestro estado local
		localStorage.setItem('lastView', event);
		setLastView(event);
	};

	return (
		<>
			<Navbar />
			{/* usamos nuestro componente calendario, enviamos nuestro localizer, nuestros eventos
			y las otras propiedades que vienen en la documentacion de la librería */}
			<Calendar
				localizer={localizer}
				events={events}
				//aqui asignamos el valor de la vista que tendrá nuestro calendario
				//que guardamos en el estado local, que a su vez tomamos de nuestro localStorage
				defaultView={lastView}
				startAccessor="start"
				endAccessor="end"
				style={{ height: 'calc(100dvh - 80px)' }}
				//mediante culture indicamos el idioma que tendrá nuestro calendario
				//el valor que asignamos debe estar en la propiedad locales, del objeto que enviamos aqui
				//que es localizer, en ese objeto tenemos la propiedad es que es la que usamos y es la que tiene los
				//textos en español de la libreria datefns
				culture="es"
				//en messages asignamos los textos de los controles del calendario, es decir
				//los textos que aparecen en los botones de navegacion del calendario
				//estos textos los obtenemos de la funcion getMessagesES que nosotros creamos y definimos
				//sus valores que retorna
				messages={getMessagesES()}
				//con eventPropGetter podemos asignar estilos a los eventos de nuestro calendario
				//en este caso usamos la funcion eventStyleGetter que nosotros creamos y definimos
				eventPropGetter={eventStyleGetter}
				//con la propiedad components, sobreescribimos el componente que muestra la información del evento
				//por uno personalizado que creamos, mandamos nuestro componente dentro de la propiedad event,
				//dentro de este objeto de components, ya que event es el que queremos sobreescribir pero no es el único que se
				//puede sobreescribir, el componente event es el que muestra el título del evento
				//al usar este componente automaticamente se envian las props que pasamos a este componente de calendar, es decir
				//que nuestro componente personalizado de event, recibe las propiedades en este caso de nuestro objeto events
				//que declaramos aqui arribita
				components={{
					event: CalendarEvent,
				}}
				//con estos eventos podemos ejecutar acciones cuando el usuario haga click, doble click o cambie la vista
				onDoubleClickEvent={onDoubleClick}
				//el select es el equivalente al click pero en esta libreria del calendario así le pusieron
				onSelectEvent={onSelect}
				onView={onViewChanged}
			/>

			<CalendarModal />
			<FabAddNew />
			<FabDelete/>
		</>
	);
};

export default CalendarPage;
