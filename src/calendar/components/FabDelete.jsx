import { useCalendarStore } from '../../hooks';

//creamos nuestro componente del botón flotante
export const FabDelete = () => {
	//importamos las acciones de eliminar y de saber si hay un evento seleccionado
	//de nuestro hook useCalendarStore
	const { startDeletingEvent, hasEventSelected } = useCalendarStore();

	//creamos nuestro método para manejar la eliminación que simplemente llama a la acción
	//de eliminar evento de nuestro hook useCalendarStore
	const handleDelete = () => {
		startDeletingEvent();
	};

	return (
		//creamos el botón flotante con el icono de borrar, que tomamos de fontawesome
		//y le asignamos el evento handleDelete al hacer click
		<button
			//asignamos nuestros estilos personalizados de nuestro archivo css usando las clases
			className="btn btn-danger fab-danger"
			onClick={handleDelete}
			//hacemos que el botón sea visible solo si hay un evento seleccionado
			//por medio del display
			style={{
				display: hasEventSelected ? '' : 'none',
			}}
		>
			<i className="fas fa-trash-alt"></i>
		</button>
	);
};
