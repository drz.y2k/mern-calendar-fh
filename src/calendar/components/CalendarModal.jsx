//importamos la funcion addHours para añadir horas a una fecha
import { addHours, differenceInSeconds } from 'date-fns';
//usamos usememo para guardar un valor en cache y que solo cambie
//cuando las dependencias cambien
import { useEffect, useMemo, useState } from 'react';
//importamos el modal de la libreria react-modal
import Modal from 'react-modal';
//importamos el datepicker de la libreria react-datepicker, y sus estilos
//de acuerdo a al documentación que vimos en la página de la libreria
//importamos la función registerLocale para registrar nuestro idioma español
//en nuestra librería y poder usar los textos en español
import DatePicker, { registerLocale } from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';

//importamos el idioma español de date-fns, esto viene en la documentación
//de la libreria react-datepicker
import es from 'date-fns/locale/es';
//importamos sweet alert, y sus estilos
import Swal from 'sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

//importamos nuestro hook useCalendarStore y useUiStore
import { useCalendarStore, useUiStore } from '../../hooks';

//el valor que mandamos como string aquí será el que debemos usar
//en nuestro DatePicker
registerLocale('es', es);

//necesitamos definir ciertos estilos, estos vienen en la documentación
//de la libreria react-modal, aquí podemos cambiarlos si queremos
//que el modal se muestre en otra ubicación por ejemplo
const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
	},
	//este estilo lo definimos nosotros para que el modal se vea por encima de todo
	overlay: {
		zIndex: 4,
	},
};

//necesitamos definir que el modal
//se va a montar en el elemento con id root
//que es el que nos crea vite para montar nuestra aplicación
Modal.setAppElement('#root');

export const CalendarModal = () => {
	//importamos las funciones que necesitamos de nuestro hook useUiStore
	//en este caso solo necesitamos saber si el modal está abierto o cerrado
	//y la función para cerrar el modal
	const { isDateModalOpen, closeDateModal } = useUiStore();

	//de nuestro store useCalendar obtenemos el evento activo
	//recordar que este es un hook que se encarga de obtener el estado de redux
	const { activeEvent, startSavingEvent } = useCalendarStore();

	//ya no necesitaremos el estado local isOpen porque tenemos el estado global
	//con nuestro hook useUiStore
	//creamos un estado para manejar si el modal está abierto o cerrado
	// const [isOpen, setIsOpen] = useState(true);
	//creamos un estado para saber si el formulario fue enviado
	//esto para mostrar los errores en los inputs
	//solo cuando se envie el formulario y los datos estén mal
	const [formSubmitted, setFormSubmitted] = useState(false);

	//creamos este estado para manejar los valores del formulario,
	//y definimos sus valores iniciales
	const [formValues, setFormValues] = useState({
		title: 'Evento',
		notes: 'Herrera',
		start: new Date(),
		end: addHours(new Date(), 2),
	});

	//este memo retorna el valor de la clase que se le debe asignar al input
	const titleClass = useMemo(() => {
		//si el formulario no ha sido enviado retornamos un string vacío
		//osea una clase vacía
		if (!formSubmitted) {
			return '';
		}
		//caso contrario, si el formulario fue enviado
		//y la longitud del titulo es mayor a 0 retornamos la clase is-valid
		//de bootstrap, si no retornamos la clase is-invalid
		//esta variable titleClass se agregará a nuestro className
		//para que se muestre el input con el borde verde o rojo

		return formValues.title.length > 0 ? 'is-valid' : 'is-invalid';

		//agregamos las dependencias de nuestro memo
		//que son el formValues.title y formSubmitted
	}, [formValues.title, formSubmitted]);

	useEffect(() => {
		//si el evento activo no es nulo
		//lo asignamos al estado que contiene los valores de nuestro formulario
		//que es el que mostramos en el modal, 
		//esta accion se ejecutará cada que cambie el evento activo
		//le asignamos esta validación porque los efectos se ejecutan al montar el componente
		//y luego cada que el arreglo de dependencias cambie, pero si asignamos null
		//a los valores del formulario, se mostrará un error en la consola
		if(activeEvent!=null){
			//aplicamos la desestructuración para asignar los valores del evento activo
			//y romper las referencias, creando con esto una copia del objeto
			setFormValues({...activeEvent});
		}
	}, [activeEvent]);

	//creamos estas funciones para manejar los cambios en los inputs
	//y en los datepickers, este primer método maneja los cambios en los inputs
	//donde tenemos el target name y value
	const onInputChange = ({ target: { name, value } }) => {
		setFormValues({
			//desestructuramos los valores actuales del formulario
			//para solo cambiar el valor que estamos modificando
			//y cuyos datos vienen en el evento
			...formValues,
			[name]: value,
		});
	};

	//hacemos lo mismo que la función de arriba pero este funcionara para los datepickers
	//ya que los date pickers emiten evento pero no tienen un target con name y value
	const onDateChanged = (event, changing) => {
		setFormValues({
			...formValues,
			[changing]: event,
		});
	};

	//creamos un método para cerrar el modal, cambiando el estado de isOpen a false
	const onCloseModal = () => {
		// setIsOpen(false);
		//usamos la función closeDateModal que obtuvimos de nuestro hook useUiStore
		//que modifica nuestro estado global para cerrar el modal
		closeDateModal();
	};

	//creamos esta función para manejar el submit
	const onSubmit = async(event) => {
		//lo primero que hacemos es prevenir el comportamiento por defecto del formulario
		//para que no se recargue la página
		event.preventDefault();
		//cambiamos el estado de formSubmitted a true
		setFormSubmitted(true);

		//usamos la función differenceInSeconds de date-fns para calcular la diferencia
		//en segundos entre las fechas de inicio y fin, si la diferencia es menor o igual a 0
		//es porque la fecha que se agregó como fecha final es menor a la fecha de inicio
		//entonces es un error, debemos retorar el mensaje de error en consola,
		//si la diferencia es NaN es porque no se ha seleccionado una fecha
		//entonces también retornamos un error

		const difference = differenceInSeconds(
			formValues.end,
			formValues.start
		);
		if (isNaN(difference) || difference <= 0) {
			console.log('error en fechas');
			//mostramos un mensaje de error con sweet alert
			//y lo configuramos para que sea mensaje de error
			Swal.fire(
				'Fechas incorrectas',
				'revisar las fechas ingresadas',
				'error'
			);
			return;
		}

		//si la longitud del titulo es menor o igual a 0 retornamos un error
		//porque significa que no se ha ingresado un titulo
		if (formValues.title.length <= 0) return;

		//si todo sale bien mostramos los valores del formulario en consola
		console.log(formValues);

		//TODO
		//cerrar modal
		//guardamos en nuestro store los valores del formulario
		//usando el método del hook
		//cerramos nuestro modal y asignamos false a formSubmitted
		//para que no se quede en true y se muestren los errores
		await startSavingEvent(formValues);
		closeDateModal();
		setFormSubmitted(false);
		//remover errores en pantalla
	};

	return (
		<Modal
			//definimos si el modal está abierto o cerrado con el estado local isOpen
			// isOpen={isOpen}
			//usamos el estado global para saber si el modal está abierto o cerrado
			isOpen={isDateModalOpen}
			//funcion que se ejecuta cuando se cierra el modal, como cuando se hace click en el overlay
			onRequestClose={onCloseModal}
			//estilos personalizados definidos por nosotros presentes en la documentación de la libreria
			style={customStyles}
			contentLabel="Example Modal"
			//definimos la clase para nuestro modal
			className="modal"
			//y definimos la clase para el overlay que es el fondo del modal
			overlayClassName="modal-fondo"
			//definimos el tiempo que tarda en cerrarse el modal
			//en este caso 200 milisegundos, ya que es el mismo tiempo que tenemos en la animación
			//de nuestro estilo de css
			closeTimeoutMS={200}
		>
			{/* contenido del modal */}
			{/* este codigo de html fue proporcionado por fernando herrera */}
			<h1> Nuevo evento </h1>
			<hr />
			{/* cuando hagan submit al formulario llamamos nuestra función onSubmit */}
			<form className="container" onSubmit={onSubmit}>
				<div className="form-group mb-2">
					<label>Fecha y hora inicio</label>
					{/* importamos nuestro datepicker y agregamos
					una fecha seleccionada inicialmente, y agregamos el evento onchange, propio
					de la libreria, en event solo va el valor de la fecha
					así que indicamos cual fecha se va a modificar, aquí onDateChanged
					es la función que nosotros creamos arriba */}
					<DatePicker
						selected={formValues.start}
						onChange={(event) => onDateChanged(event, 'start')}
						className="form-control"
						dateFormat="Pp"
						//con showtimeselect mostramos el selector de hora
						showTimeSelect
						//elegimos el idioma que tendrá el componente que será de los que registramos arriba
						//con registerLocale
						locale="es"
						//definimos el mensaje que tendrá el selector de hora
						timeCaption="Hora"
					/>
				</div>

				<div className="form-group mb-2">
					<label>Fecha y hora fin</label>
					<DatePicker
						// lo mismo que la función de arriba pero este sera para la fecha de fin
						minDate={formValues.start}
						selected={formValues.end}
						onChange={(event) => onDateChanged(event, 'end')}
						className="form-control"
						dateFormat="Pp"
						showTimeSelect
						locale="es"
						timeCaption="Hora"
					/>
				</div>

				<hr />
				<div className="form-group mb-2">
					<label>Titulo y notas</label>
					<input
						type="text"
						//agregamos la clase que definimos en nuestro memo
						//recordar que si el formulario no ha sido enviado
						//la clase será vacía, caso contrario
						//será is-valid o is-invalid dependiendo de si el titulo
						//tiene contenido o no
						className={`form-control ${titleClass}`}
						placeholder="Título del evento"
						name="title"
						autoComplete="off"
						//definimos el valor del input con el valor del formulario
						value={formValues.title}
						//definimos el evento que se ejecuta cuando cambia el valor del input
						onChange={onInputChange}
					/>
					<small id="emailHelp" className="form-text text-muted">
						Una descripción corta
					</small>
				</div>

				<div className="form-group mb-2">
					<textarea
						type="text"
						className="form-control"
						placeholder="Notas"
						rows="5"
						name="notes"
						value={formValues.notes}
						onChange={onInputChange}
					></textarea>
					<small id="emailHelp" className="form-text text-muted">
						Información adicional
					</small>
				</div>

				<button
					type="submit"
					className="btn btn-outline-primary btn-block"
				>
					<i className="far fa-save"></i>
					<span> Guardar</span>
				</button>
			</form>
		</Modal>
	);
};
