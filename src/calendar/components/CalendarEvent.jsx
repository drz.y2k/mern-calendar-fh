/* eslint-disable react/prop-types */ 
//se podria memoizar en el caso de tener muchos muchos eventos
export const CalendarEvent = (props) => {
	const { title, user } = props.event;
	//si tenemos dudas de las props siempre podemos imprimir en consola
	// console.log(props);

	return (
		// este es nuestro componente personalizado que se va a mostrar en el calendario
		//estamos mostrando el titulo y el nombre del usuario, estas props llegan de forma automatica 
		//a este componente enviados por el componente Calendar de la libreria
		//react-big-calendar, entonces aqui las podemos recibir y empezar a usar
		//en este caso queremos que el titulo del evento esté en negrita y el nombre del usuario que la creó esté
		//en texto normal por eso hacemos esto
		<>
			<strong>{title}</strong>
			<span> - {user.name}</span>
		</>
	);
};
