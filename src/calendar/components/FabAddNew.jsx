import { addHours } from 'date-fns';
import { useCalendarStore, useUiStore } from '../../hooks';

//creamos el componente FabAddNew que es un botón flotante que al hacer click
//abre el modal de evento para agregar uno nuevo
export const FabAddNew = () => {
	//hacemos uso de los 2 hooks que tenemos
	//openDateModal es para abrir el modal de fecha
	//setActiveEvent es para asignar el evento activo en el calendario

	const { openDateModal } = useUiStore();
	const { setActiveEvent } = useCalendarStore();

	//creamos la función handleClickNew que se ejecuta al hacer click en el botón
	//asignamos un evento vacío a la propiedad activeEvent, esta estructura es la misma que tenemos
	//en nuestro store pero aquí la usamos para crear un evento vacío
	//la sección user se agregará posteriormente cuando se conecte con el backend
	const handleClickNew = () => {
		setActiveEvent({
			title: '',
			notes: '',
			start: new Date(),
			end: addHours(new Date(), 2),
			bgColor: '#fafafa',
			user: {
				_id: '123',
				name: 'Fernando',
			},
		});
		//llamamos la función para abrir el modal de evento
		openDateModal();
	};

	return (
		//creamos el botón flotante con el icono de más, que tomamos de fontawesome
		//y le asignamos el evento handleClickNew al hacer click
		<button className="btn btn-primary fab" onClick={handleClickNew}>
			<i className="fas fa-plus"></i>
		</button>
	);
};
