//creamos nuestro slice de redux
import { createSlice } from "@reduxjs/toolkit";
//importamos esta función porque la necesitamos en nuestro evento temporal de prueba
import { addHours } from "date-fns";

//creamos un evento temporal de prueba, esta información en un futuro vendrá del backend,
//de momento solo se hace de ejemplo
const tempEvent = {
	//vamos a agregar un id a nuestro evento temporal de acuerdo a como lo vamos a manejar en el back
	//de momento vamos a simular que el id es un timestamp con este código para obtener
	//el timestamp de la fecha actual
	_id: new Date().getTime(),
	title: 'Cumpleaños del jefe',
	notes: 'Hay que comprar el pastel',
	start: new Date(),
	end: addHours(new Date(), 2),
	bgColor: '#fafafa',
	user: {
		_id: '123',
		name: 'Fernando',
	},
};

//creamos nuestro slice de redux
//asignamos nombre, creamos nuestro arreglo de eventos con el evento temporal de prueba
//y creamos la propiedad activeEvent que será el evento activo en el calendario
export const calendarSlice = createSlice({
	name: "calendar",
	initialState: {
		events: [
			tempEvent
		],
		activeEvent: null,
	},
	//este reducer se va a cambiar, ahorita solo está de ejemplo
	reducers: {
		//state, action
		//vamos a asignar nuestro evento activo con esta función
		//recibe el state para modificarlo, y de la action desestructuramos el payload que lo asignaremos 
		//a la propiedad activeEvent
		onSetActiveEvent: (state, { payload }) => {
			state.activeEvent = payload;
		},

		//creamos un nuevo reducer para agregar un nuevo evento
		//el evento lo recibimos como payload y lo agregamos al arreglo de eventos
		//mediante el push, posteriormente asignamos null al evento activo
		onAddNewEvent: (state, { payload }) => {
			state.events.push(payload);
			state.activeEvent = null;
		},

		//creamos la función onUpdateEvent
		//esta recibe el payload que será el evento modificado que asignaremos en nuestro store
		onUpdateEvent: (state, { payload }) => {
			//hacemos un map de nuestros eventos
			//si el id del evento que estamos iterando es igual al id del evento que queremos modificar
			//que enviamos en nuestro payload, entonces retornamos el payload
			//con esto se modificará del arreglo el evento que coincide con el id que enviamos
			//con el contenido del payload, y todos los demás eventos se mantendrán igual
			state.events = state.events.map(event => {
				if (event._id === payload._id) {
					return payload;
				}
				return event;
			})
		},

		//creamos la función onDeleteEvent
		//esta valida que haya un evento activo, si existe
		//procede a hacer la eliminación por medio de un filter, 
		//haciendo que nuestros eventos sean todos los eventos que no coincidan con el id del evento activo
		//osea va a eliminar el evento activo, y posteriormente asigna null al evento activo
		//ya que ahí estaba una copia del evento que eliminamos
		onDeleteEvent: (state) => {
			if (state.activeEvent) {
				//no podemos hacer return aquí para validar esto, porque sería retornar un nuevo
				//estado de nuestro store así que forzosamente hacemos una asignación en el store
				state.events = state.events.filter(event => event._id !== state.activeEvent._id);
				state.activeEvent = null;
			}
		}
	}
});

//exportamos el reducer
export const { onSetActiveEvent, onAddNewEvent, onUpdateEvent, onDeleteEvent } = calendarSlice.actions;