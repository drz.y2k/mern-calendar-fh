import { createSlice } from '@reduxjs/toolkit';

//creamos nuestro slice de toolkit
export const uiSlice = createSlice({
	//asignamos un nombre a nuestro slice
	name: 'ui',
	//configuramos un estado inicial con un objeto
	initialState: {
		isDateModalOpen: false,
	},
	//configuramos nuestros reducers
	//que mutarán nuestro estado
	reducers: {
		//debemos pasar el estado como primer argumento
		//para poder editarlo
		onOpenDateModal: (state) => {
			state.isDateModalOpen = true;
			//sino estuvieramos usando el toolkit esta mutación sería más engorrosa
			//algo como
			//return {
			//	...state,
			//	isDateModalOpen: true,
			//}
		},
		onCloseDateModal: (state) => {
			state.isDateModalOpen = false;
		},
		
	},
});

//exportamos nuestros reducers
export const { onOpenDateModal, onCloseDateModal } = uiSlice.actions;