import { configureStore } from "@reduxjs/toolkit";
//importamos nuestros slices de la ui
import { uiSlice, calendarSlice } from "./";

//configuramos nuestra store con el reducer de uiSlice
//que importamos previamente y le ponemos el nombre de ui
//este nombre es el que va a aparecer en nuestras devtools de redux
export const store = configureStore({
	reducer: {
		ui: uiSlice.reducer,
		//usamos nuestro reducer de calendar de nuestro slice
		calendar: calendarSlice.reducer,
	},
	//realizamos esta configuración para que no nos de advertencias en la consola
	//aqui quitamos el chequeo de serializacion que hace redux para ver cuando
	//datos que tenemos serializables no los mandamos serializados
	//esto está pasando porque estamos mandando los campos de tipo fecha

	//dicho por IA
	// La opción serializableCheck: false desactiva la verificación de que todas 
	//las acciones y el estado sean serializables. Esto puede ser útil si estás interactuando 
	//con datos no serializables, como objetos Date.
	middleware: (getDefaultMiddleware) => getDefaultMiddleware({
		serializableCheck: false,
	}),
})